import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {

    private static String[] columns = {"Mobile Number", "International Format",
                                        "Operator Name"};
    private static List<MobileNumbers> no = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        String path = "D:\\CODES\\java\\DIT\\MobileNumberIdentifier\\";
        FileInputStream fin = new FileInputStream(path + "test1.xlsx");

        ArrayList<String> internationalNumbers;
        ArrayList<String> operatorName;
        ArrayList<String> mobileNumbers;

        // For accessing values from external excel file
        XSSFWorkbook workbook = new XSSFWorkbook(fin);
        XSSFSheet sheet = workbook.getSheetAt(0);

        Processing.processing(sheet);
        mobileNumbers = Processing.orgValues(sheet);
        internationalNumbers = Processing.processing(sheet);
        operatorName = Processing.operator(internationalNumbers);

        Iterator mobItr = mobileNumbers.iterator();
        Iterator noItr = internationalNumbers.iterator();
        Iterator opItr = operatorName.iterator();

        // Adding values into MobileNumbers objects
        while (mobItr.hasNext()) {
            no.add(new MobileNumbers(mobItr.next().toString(),
                    noItr.next().toString(), opItr.next().toString()));
        }

        // New Excel File
        Workbook workbook1 = new XSSFWorkbook();
        Sheet sheet1 = workbook1.createSheet("MobileNo");

        // Font Property
        Font headerFont = workbook1.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook1.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Creating Row
        Row headerRow = sheet1.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Creating other rows and cess with Mobile# data
        int rowNum = 1;

        // mobile# in no ArrayList<>() top
        for (MobileNumbers mobile : no) {
            Row row = sheet1.createRow(rowNum++);
            row.createCell(0).setCellValue(mobile.getMobileNo());
            row.createCell(1).setCellValue(mobile.getInternationalNo());
            row.createCell(2).setCellValue(mobile.getOperator());
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet1.autoSizeColumn(i);
        }

        fin.close();

        // Fail-safe
        File check = new File(path + "MobileConversion.xlsx");
        FileOutputStream fout = null;
        if (check.exists()) {
            System.out.println("File already exists");
        } else {
            fout = new FileOutputStream(path + "MobileConversion.xlsx");
            workbook1.write(fout);
            fout.close();
        }
        System.out.println("Conversion completed.");
    }

}

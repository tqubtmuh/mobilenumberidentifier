import org.apache.poi.xssf.usermodel.XSSFSheet;
import java.util.ArrayList;

public class Processing {

    // Add 255 for numbers beginning with 0
    private static String insertString(String ogString, String newString, int pos) {
        return ogString.substring(0, pos + 1) + newString + ogString.substring(pos + 1);
    }

    // Get the 1st column of values on give excel sheet(test1.xlsx)
    public static ArrayList<String> orgValues(XSSFSheet ogSheet) {
        ArrayList<String> mobNumbers = new ArrayList<>();
        for (int i = 1; i <= ogSheet.getLastRowNum(); i++) {
            String value = ogSheet.getRow(i).getCell(0).getStringCellValue();
            mobNumbers.add(value);
        }
        return mobNumbers;
    }

    // Removes unnecessary characters from given numbers
    public static ArrayList<String> processing(XSSFSheet sheet) {
        ArrayList<String> internationalNumber = new ArrayList<>();
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            String value = sheet.getRow(i).getCell(0).getStringCellValue();
            String newValue = value.replaceAll("\\s", "");
            String newValue1 = newValue.replaceAll("\\D", "");
            String intValue = "255";
            int index = -1;

            if (newValue1.charAt(0) == '0') {
                String rString = newValue1.substring(1);
                internationalNumber.add(insertString(rString, intValue, index));
            } else {
                internationalNumber.add(newValue1);
            }
        }
        return internationalNumber;
    }

    // Provides the list of operators depending on # type
    public static ArrayList<String> operator(ArrayList<String> iNumbers) {
        ArrayList<String> intNumber = new ArrayList<>();
        ArrayList<String> operator = new ArrayList<>();

        intNumber.addAll(iNumbers);

        for (String iNumber: intNumber) {
            if ((iNumber.subSequence(3, 5).equals("78")) || (iNumber.subSequence(3, 5).equals("68"))
                || (iNumber.subSequence(3, 5).equals("69"))) {
                operator.add("Airtel");
            } else if ((iNumber.subSequence(3, 5).equals("62"))) {
                operator.add("Halotel");
            } else if ((iNumber.subSequence(3, 5).equals("79"))) {
                operator.add("Smart");
            } else if ((iNumber.subSequence(3, 5).equals("66"))) {
                operator.add("Smile");
            } else if ((iNumber.subSequence(3, 5).equals("73"))) {
                operator.add("TTCL");
            } else if ((iNumber.subSequence(3, 5).equals("65")) || (iNumber.subSequence(3, 5).equals("67"))
                    || (iNumber.subSequence(3, 5).equals("71")))  {
                operator.add("Tigo");
            } else if ((iNumber.subSequence(3, 5).equals("74")) || (iNumber.subSequence(3, 5).equals("75"))
                    || (iNumber.subSequence(3, 5).equals("76")))  {
                operator.add("Vodacom");
            } else if ((iNumber.subSequence(3, 5).equals("77"))) {
                operator.add("Zantel");
            } else {
                operator.add("Unknown");
            }
        }
        return operator;
    }
}
